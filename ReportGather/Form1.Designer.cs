﻿namespace ReportGather
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.buttonAddFolders = new System.Windows.Forms.Button();
			this.foldersListView = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.buttonGather = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonAddFolders
			// 
			this.buttonAddFolders.Location = new System.Drawing.Point(716, 13);
			this.buttonAddFolders.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.buttonAddFolders.Name = "buttonAddFolders";
			this.buttonAddFolders.Size = new System.Drawing.Size(62, 28);
			this.buttonAddFolders.TabIndex = 0;
			this.buttonAddFolders.Text = "+";
			this.buttonAddFolders.UseVisualStyleBackColor = true;
			this.buttonAddFolders.Click += new System.EventHandler(this.buttonAddFolders_Click);
			// 
			// foldersListView
			// 
			this.foldersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
			this.foldersListView.FullRowSelect = true;
			this.foldersListView.Location = new System.Drawing.Point(14, 13);
			this.foldersListView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.foldersListView.Name = "foldersListView";
			this.foldersListView.Size = new System.Drawing.Size(696, 199);
			this.foldersListView.TabIndex = 1;
			this.foldersListView.UseCompatibleStateImageBehavior = false;
			this.foldersListView.View = System.Windows.Forms.View.Details;
			this.foldersListView.SelectedIndexChanged += new System.EventHandler(this.foldersListView_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Folders";
			this.columnHeader1.Width = 460;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Status";
			this.columnHeader2.Width = 95;
			// 
			// buttonGather
			// 
			this.buttonGather.Enabled = false;
			this.buttonGather.Location = new System.Drawing.Point(14, 219);
			this.buttonGather.Name = "buttonGather";
			this.buttonGather.Size = new System.Drawing.Size(75, 23);
			this.buttonGather.TabIndex = 2;
			this.buttonGather.Text = "Gather";
			this.buttonGather.UseVisualStyleBackColor = true;
			this.buttonGather.Click += new System.EventHandler(this.buttonGather_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(717, 49);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(61, 28);
			this.buttonClear.TabIndex = 3;
			this.buttonClear.Text = "Clear";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(790, 263);
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.buttonGather);
			this.Controls.Add(this.foldersListView);
			this.Controls.Add(this.buttonAddFolders);
			this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "MainForm";
			this.Text = "Report Gather";
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAddFolders;
        private System.Windows.Forms.ListView foldersListView;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.Button buttonGather;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.Button buttonClear;
    }
}

