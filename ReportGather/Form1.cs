﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using Excel = Microsoft.Office.Interop.Excel;

namespace ReportGather
{
	public partial class MainForm : Form
	{
		private string _szLastSelectedFolder = "";

		public MainForm()
		{
			InitializeComponent();

			foldersListView.Columns[foldersListView.Columns.Count - 1].Width = -2;
		}

		// TODO :

		private void buttonAddFolders_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.ShowNewFolderButton = false;

			if (!string.IsNullOrWhiteSpace(_szLastSelectedFolder))
			{
				folderBrowserDialog.SelectedPath = _szLastSelectedFolder;
			}

			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				if (!string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
				{
					ListViewItem lvi = foldersListView.Items.Add(folderBrowserDialog.SelectedPath);
					lvi.SubItems.Add("");

					_szLastSelectedFolder = folderBrowserDialog.SelectedPath;
				}

				if (foldersListView.Items.Count > 0)
					buttonGather.Enabled = true;
			}
		}

		private void foldersListView_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void buttonGather_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < foldersListView.Items.Count; i++)
			{
				string szResult = GatherFolderReport(foldersListView.Items[i].SubItems[0].Text);
				foldersListView.Items[i].SubItems[1].Text = szResult;
			}

			Excel.Application oXL;
			Excel.Workbook oWB_Final, oWB_Report;
			Excel.Worksheet oWS_Final, oWS_Report;

			oXL = new Excel.Application();
			oXL.DisplayAlerts = false;
			oXL.Visible = false;

			oWB_Final = oXL.Application.Workbooks.Add();
			for (int i = 0; i < foldersListView.Items.Count; i++)
			{
				string szReportFilename = GetReportFilename(foldersListView.Items[i].SubItems[0].Text);

				if (szReportFilename != "")
				{
					string szFolderName = GetFolderNmae(szReportFilename);

					oWB_Report = oXL.Workbooks.Open(szReportFilename);
					oWS_Report = oWB_Report.Sheets["report"];

					oWS_Report.Copy(Type.Missing, oWB_Final.Sheets[oWB_Final.Sheets.Count]);

					oWB_Report.Close();

					//oWB_Final.Sheets[oWB_Final.Sheets.Count];
					oWS_Final = oWB_Final.Sheets[oWB_Final.Sheets.Count];
					oWS_Final.Name = szFolderName;
				}
			}

			if (oWB_Final.Sheets.Count > 3)
			{
				oWS_Report = oWB_Final.Sheets["Sheet1"];
				oWS_Report.Delete();
				oWS_Report = oWB_Final.Sheets["Sheet2"];
				oWS_Report.Delete();
				oWS_Report = oWB_Final.Sheets["Sheet3"];
				oWS_Report.Delete();

				SaveFileDialog saveFileDialog1 = new SaveFileDialog();
				saveFileDialog1.Filter = "Excel file|*.xlsx";
				saveFileDialog1.Title = "Save report file";
				saveFileDialog1.ShowDialog();

				if (saveFileDialog1.FileName != "")
				{
					oWB_Final.SaveAs(saveFileDialog1.FileName);
				}
			}

			oWB_Final.Close();

			oXL.UserControl = true;
			oXL.Application.Quit();
		}

		private string GatherFolderReport(string szFolder)
		{
			Excel.Application oXL;
			Excel.Workbook oWB_Report, oWB_GT, oWB_Track;
			Excel.Worksheet oWS_Report, oWS_GT, oWS_Track;
			Excel.Range finalCell;
			int iTotalColumns, iTotalRows;

			string szReportFilename = GetReportFilename(szFolder);
			string szGroundTruthFilename = GetGroundTruthFilename(szFolder);
			string szTrackResultFilename = GetTrackResultFilename(szFolder);

			if (szReportFilename == "" )
			{
				return "Can't find 0-report*.xlsx";
			}
			if (szGroundTruthFilename == "")
			{
				return "Can't find 0-result-ground-truth-*.csv";
			}
			if( szTrackResultFilename == "")
			{
				return "Can't find 0-result-track-box-*.csv";
			}

			oXL = new Excel.Application();
			oXL.DisplayAlerts = false;
			oXL.Visible = false;

			oWB_Report = oXL.Workbooks.Open(szReportFilename);
			oWB_GT = oXL.Workbooks.Open(szGroundTruthFilename);
			oWB_Track = oXL.Workbooks.Open(szTrackResultFilename);

			// ground truth
			oWS_Report = oWB_Report.Sheets["ground-truth"];
			iTotalColumns = oWS_Report.UsedRange.Columns.Count;
			iTotalRows = oWS_Report.UsedRange.Rows.Count;
			finalCell = oWS_Report.Cells[iTotalRows, iTotalColumns];
			oWS_Report.get_Range("A1", finalCell).Clear();

			oWS_GT = oWB_GT.ActiveSheet;
			iTotalColumns = oWS_GT.UsedRange.Columns.Count;
			iTotalRows = oWS_GT.UsedRange.Rows.Count;
			finalCell = oWS_GT.Cells[iTotalRows, iTotalColumns];
			oWS_GT.get_Range("A1", finalCell).Copy(Type.Missing);

			oWS_Report.get_Range("A1").PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormulas, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);

			oWB_GT.Close();

			// track result
			oWS_Report = oWB_Report.Sheets["track-result"];
			iTotalColumns = oWS_Report.UsedRange.Columns.Count;
			iTotalRows = oWS_Report.UsedRange.Rows.Count;
			finalCell = oWS_Report.Cells[iTotalRows, iTotalColumns];
			oWS_Report.get_Range("A1", finalCell).Clear();

			oWS_Track = oWB_Track.ActiveSheet;

			iTotalColumns = oWS_Track.UsedRange.Columns.Count;
			iTotalRows = oWS_Track.UsedRange.Rows.Count;
			finalCell = oWS_Track.Cells[iTotalRows, iTotalColumns];
			oWS_Track.get_Range("A1", finalCell).Copy(Type.Missing);

			oWS_Report.get_Range("A1").PasteSpecial(Microsoft.Office.Interop.Excel.XlPasteType.xlPasteFormulas, Microsoft.Office.Interop.Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
			
			oWB_Track.Close();

			oWB_Report.Save();

			oWB_Report.Close();

			oXL.UserControl = true;

			oXL.Application.Quit();

			return "Processed";
		}

		private string GetReportFilename(string szFolder)
		{
			string[] fileArray = Directory.GetFiles(szFolder, "0-report*.xlsx");
			if (fileArray.Length > 0)
				return fileArray[0];
			else
				return "";
		}

		private string GetTrackResultFilename(string szFolder)
		{
			string[] fileArray = Directory.GetFiles(szFolder, "0-result-track-box-*.csv");
			if (fileArray.Length > 0)
				return fileArray[0];
			else
				return "";
		}

		private string GetGroundTruthFilename(string szFolder)
		{
			string[] fileArray = Directory.GetFiles(szFolder, "0-result-ground-truth-*.csv");
			if (fileArray.Length > 0)
				return fileArray[0];
			else
				return "";
		}

		private string GetFolderNmae(string szFolder)
		{
			string lastFolderName = Path.GetFileName(Path.GetDirectoryName(szFolder));

			return lastFolderName;
		}

		private void buttonClear_Click(object sender, EventArgs e)
		{
			foldersListView.Items.Clear();
			_szLastSelectedFolder = "";
		}

	}
}
